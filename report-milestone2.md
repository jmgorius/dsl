# Milestone 2 - Report

The choice has been made to omit the *Constant* rule, as the reduction of a formula using constants
to one using only variables is easily carried over.

## Well-formed rules not expressed in the grammar

The grammar does not express the following well-formed rules:
* variables identifiers should only contain letters and digits and cannot begin with a digit;
* keywords should not be used as variable names.

## Metamodel comparison

The metamodel generated using the XText grammar is more general than the one given in milestone 1, in the sense
that unary and binary operators are not distinguished. Each and every component of a formula is described by
zero or one left hand side, zero or one right hand side and an identifier.
The structural representation of a formula is more explicit in the milestone 1 metamodel.

## Grammar comparison

The grammar generated from the milestone 1 metamodel does not express the concrete syntax of the SAT formula
language. In fact, it uses a default syntax wich resorts to curly braces to create nested constructs.
Moreover, our XText grammar does express operator precedence rules, which do not appear in the generated
grammar.
