package org.sif.dsl.launcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.debug.ui.ILaunchShortcut;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.sif.dsl.launcher.SolverLauncher;

@SuppressWarnings("all")
public class LaunchShortcut implements ILaunchShortcut {
  private IFile currFile;
  
  @Override
  public void launch(final ISelection selection, final String mode) {
    if ((selection instanceof IStructuredSelection)) {
      final IStructuredSelection structuredSelection = ((IStructuredSelection) selection);
      final Object object = structuredSelection.getFirstElement();
      if ((object instanceof IAdaptable)) {
        IResource _adapter = ((IAdaptable) object).<IResource>getAdapter(IResource.class);
        this.currFile = ((IFile) _adapter);
        InputStream in = null;
        try {
          in = this.currFile.getContents();
        } catch (final Throwable _t) {
          if (_t instanceof CoreException) {
            final CoreException e = (CoreException)_t;
            e.printStackTrace();
          } else {
            throw Exceptions.sneakyThrow(_t);
          }
        }
        InputStreamReader _inputStreamReader = new InputStreamReader(in);
        BufferedReader reader = new BufferedReader(_inputStreamReader);
        StringBuilder out = new StringBuilder();
        String line = null;
        try {
          while (((line = reader.readLine()) != null)) {
            out.append(line);
          }
        } catch (final Throwable _t_1) {
          if (_t_1 instanceof IOException) {
            final IOException e_1 = (IOException)_t_1;
            e_1.printStackTrace();
          } else {
            throw Exceptions.sneakyThrow(_t_1);
          }
        }
        this.launch(out.toString());
      }
    }
  }
  
  @Override
  public void launch(final IEditorPart editor, final String mode) {
    final IEditorInput input = editor.getEditorInput();
    IFile _adapter = input.<IFile>getAdapter(IFile.class);
    this.currFile = ((IFile) _adapter);
    InputStream in = null;
    try {
      in = this.currFile.getContents();
    } catch (final Throwable _t) {
      if (_t instanceof CoreException) {
        final CoreException e = (CoreException)_t;
        e.printStackTrace();
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
    InputStreamReader _inputStreamReader = new InputStreamReader(in);
    BufferedReader reader = new BufferedReader(_inputStreamReader);
    StringBuilder out = new StringBuilder();
    String line = null;
    try {
      while (((line = reader.readLine()) != null)) {
        out.append(line);
      }
    } catch (final Throwable _t_1) {
      if (_t_1 instanceof IOException) {
        final IOException e_1 = (IOException)_t_1;
        e_1.printStackTrace();
      } else {
        throw Exceptions.sneakyThrow(_t_1);
      }
    }
    this.launch(out.toString());
  }
  
  private void launch(final String fileContents) {
    new SolverLauncher().launch(fileContents);
  }
}
